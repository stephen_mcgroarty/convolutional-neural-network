#ifndef LINEAR_CLASSIFIER_H
#define LINEAR_CLASSIFIER_H
#include "Network.h"

class LinearClassifier: public Network
{
public:
	LinearClassifier(Network * previous);

	void Predict(std::shared_ptr<float> data, int width,int height, int depth) override;

	void Train(std::shared_ptr<float> input,std::shared_ptr<float> output, float label) override;

private:
	float Sigmoid(float z)const;
	float Hypothesis(std::shared_ptr<float> data) const;

	float Cost(std::shared_ptr<float> x,float y) const;

	int m_NumberOfWeights;
	std::unique_ptr<float> m_pWeights;
};


#endif

