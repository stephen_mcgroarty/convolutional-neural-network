#include "LinearClassifier.h"
#include <cmath>
#include <stdio.h>
#include <memory>
#include <stdlib.h>
#include <assert.h>


LinearClassifier::LinearClassifier(Network * previous):
		Network(1,1,1),
		m_NumberOfWeights(previous->GetOutputWidth()*previous->GetOutputHeight()*previous->GetOutputDepth() + 1)
{
	m_pWeights = std::unique_ptr<float>(new float[m_NumberOfWeights]);
	
	for ( int weight = 0; weight < m_NumberOfWeights; ++weight)
	{
		m_pWeights.get()[weight] = 2.0f*(static_cast<float>(rand())/static_cast<float>(RAND_MAX)) -1.0f;
	}
	m_Trainable= true;

	m_pGradients = std::shared_ptr<float>(new float[m_NumberOfWeights - 1]);
	memset(m_pGradients.get(),0,(m_NumberOfWeights -1)*sizeof(float));
}


void LinearClassifier::Predict(std::shared_ptr<float> data, int width,int height, int depth)
{
    // Only contains a single float for the prediction
    m_pOutputBuffer = std::shared_ptr<float>( new float );
	*m_pOutputBuffer = Hypothesis(data);   
}


float LinearClassifier::Sigmoid(float z) const
{
	return 1.0f/(1.0f+exp(-z));
}


float LinearClassifier::Hypothesis(std::shared_ptr<float> x ) const
{
	float z = 0.0f;
	for (int index = 1; index < m_NumberOfWeights; ++index)
	{
        assert( !isnan(m_pWeights.get()[index]) && "Weight is NaN!");
        assert( !isnan(x.get()[index-1]) && "x is NaN!");

		z += m_pWeights.get()[index]*x.get()[index-1];
	}
    assert(!isnan(z) && "Z == NAN");

    return Sigmoid(m_pWeights.get()[0] + z);
}


float LinearClassifier::Cost(std::shared_ptr<float> x,float y) const
{
    // Add a small value to make sure h is non zero
    float h = Hypothesis(x);
   	h += (1.0f-h)*0.001f - h*0.001f;
 
    if (y == 1.0f)
    {
        return -log(h);
    } else {
        return -log(1.0f-h);
    }
}



void LinearClassifier::Train(std::shared_ptr<float> data,std::shared_ptr<float> output, float y)
{
    const float learningRate = 0.01f;
    const float sigma = 0.01f;
	float * x  = data.get();

	float biasError = 0.0f;

	float h = (*output) - y;
	float average = 0.0f;
	for (int i =1; i < m_NumberOfWeights; ++i)
	{
        float error = h*x[i-1];

        assert(isfinite(error)&&"Error is not finite!");
        m_pGradients.get()[i-1] = error;
        average += error;
	}

	average /= static_cast<float>(m_NumberOfWeights-1);
    m_Error = Cost(data,y);
 
	// Non-regularised
	for (int theta = 1; theta < m_NumberOfWeights; ++theta)
	{
		m_pWeights.get()[theta] = m_pWeights.get()[theta] - learningRate*m_pGradients.get()[theta-1];
	}
    

	m_pWeights.get()[0] -= learningRate*average;
}

