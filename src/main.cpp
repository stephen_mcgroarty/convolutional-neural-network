#include <stdio.h>
#include "Network.h"
#include "lodepng.h"
#include <string>
#include <stdlib.h> 
#include <time.h>

int loadPNG(const char* filename, std::shared_ptr<float> &imageBuffer,unsigned int &width,unsigned int &height)
{
	std::vector<unsigned char> rawPixels,buffer;
	std::string tmp = filename;

	unsigned int error = 0;
	lodepng::load_file(rawPixels, filename);
	error = lodepng::decode(buffer, width, height, rawPixels);
	
	if(error)
	{
		printf("Error: %s\n",lodepng_error_text(error));
		return -1;
	}
	

	imageBuffer = std::shared_ptr<float>(new float[width*height*3]);
	memset(imageBuffer.get(),0.0f,sizeof(float)*width*height*3);
    for (int y = 0; y < height; ++y)
	{
        for (int x =0; x < width; ++x)
        {
        	int newIndex = y*width*3 +x*3;
            int index = y*width*4 + x*4;
            imageBuffer.get()[newIndex] = static_cast<float>(buffer[index])/255.0f;
            imageBuffer.get()[newIndex+1] =  static_cast<float>(buffer[index+1])/255.0f;
            imageBuffer.get()[newIndex+2] =  static_cast<float>(buffer[index+2])/255.0f;
        }
	}
	

	return 0;
}



int savePNG(const char* filename, std::shared_ptr<float> &imageBuffer,int width,int height)
{
	std::vector<unsigned char> rawPixels,buffer;
	unsigned int error = 0;

	// Convert from 0.0f-1.0f to 0-255u and add in alpha
    for (int y = 0; y < height; ++y)
    {
        for (int x =0; x < width; ++x)
        {
            int index = y*width*3 + x*3;
            buffer.push_back(imageBuffer.get()[index]*255.0f);
            buffer.push_back(imageBuffer.get()[index+1]*255.0f);
            buffer.push_back(imageBuffer.get()[index+2]*255.0f);
            buffer.push_back(255);
        }
	}
	
	error = lodepng::encode(rawPixels, buffer, width, height);
	
	if(error)
	{
		printf("Error: %s\n",lodepng_error_text(error));
		return -1;
	}

	lodepng::save_file(rawPixels,filename);
	
	return 0;
}




void toGreyscale(std::shared_ptr<float>  pixels,int width,int height,int bpp)
{    
    unsigned int imgW = width * bpp;
    
    for (int y = 0; y < height; ++y)
    {
        for (int x = 0; x < width; ++x)
        {
            int index = ( ( y * imgW ) + ( x * bpp ) );
            float greyscale = 0;
            greyscale += pixels.get()[index]*0.21;
            greyscale += pixels.get()[index+1]*0.71;
            greyscale += pixels.get()[index+2]*0.07;
            pixels.get()[index] = pixels.get()[index+1] = pixels.get()[index+2] = (unsigned char)greyscale;
        }
    }
}


int main(int argc,char ** argv)
{

	srand(time(nullptr));

	unsigned int width =0;
	unsigned int height=0;


	const char * filenames[] 
	{
		"../images/circles/Circle1.png",
		"../images/crosses/cross1.png",

		"../images/circles/Circle2.png",
		"../images/crosses/cross2.png",

		"../images/circles/Circle3.png",
		"../images/crosses/cross3.png",

		"../images/circles/Circle4.png",
		"../images/crosses/cross4.png",

		"../images/circles/Circle5.png",
		"../images/crosses/cross5.png",

		"../images/circles/Circle6.png",
		"../images/crosses/cross6.png",

		"../images/circles/Circle7.png",
		"../images/crosses/cross7.png",

		"../images/circles/Circle8.png",
		"../images/crosses/cross8.png",


		"../images/circles/Circle9.png",
		"../images/crosses/cross9.png",

		"../images/circles/Circle10.png",
		"../images/crosses/cross10.png",

		"../images/circles/Circle11.png",
		"../images/crosses/cross11.png",

		"../images/circles/Circle12.png",
		"../images/crosses/cross12.png",

		"../images/circles/Circle13.png",
		"../images/crosses/cross13.png",

		"../images/circles/Circle14.png",
		"../images/crosses/cross14.png",

		"../images/circles/Circle15.png",
		"../images/crosses/cross15.png",

		"../images/circles/Circle16.png",
		"../images/crosses/cross16.png",

		"../images/circles/Circle17.png",
		"../images/crosses/cross17.png",

		"../images/circles/Circle18.png",
		"../images/crosses/cross18.png",


		"../images/circles/Circle19.png",
		"../images/crosses/cross19.png",

		"../images/circles/Circle20.png",
		"../images/crosses/cross20.png"
	};

	std::vector<float> labels {
		1.0f,0.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		1.0f,0.0f
	};


    std::vector<std::shared_ptr<float>> images;
    
    std::shared_ptr<float> imageBuffer;

    for (int i =0; i < 40; ++i)
    {
    	loadPNG(filenames[i],imageBuffer,width,height);
    	images.push_back(imageBuffer);
    }
    int depth = 3;


    // Width, height, and depth (3 due to RGB) of input image
	Network seqNet(width,height,depth);

	// Layer 1 - Convolution - ReLu - MaxPool
	seqNet.AddConvolution(5,3,1);
	seqNet.AddMaxPooling(2);

	// Layer 2 - Convolution - ReLu - MaxPool
	seqNet.AddConvolution(3,3,1);
	seqNet.AddMaxPooling(2);

	// Layer 3 - Linear Classifier
	seqNet.AddLinearClassifier();

    static const int PRINT_AFTER_ITERATION_NUMBER = 100;
	for ( int i =0; i < 500; ++i)
	{	
		float numberCorrect = 0.0f;
        for ( int example = 0; example < images.size(); ++example)
        {
        	if ( !(i % PRINT_AFTER_ITERATION_NUMBER) )
        	{
        		seqNet.Predict(images[example],width,height,3);
            	int outWidth = 0;
            	int outHeight = 0;
            	int outDepth =0;
            	std::shared_ptr<float> output = seqNet.GetOutput(outWidth,outHeight,outDepth);

            	printf("Exampe %s, iteration %d, prediction = %f actual = %f\n",filenames[example],i,*output,labels[example]);

        		if ( *output > 0.5f == labels[example] > 0.5f )
        		{
        			numberCorrect += 1.0f;
        		}
        	}
        	else 
        	{
        		seqNet.Train(images[example],width,height,3,labels[example]);
        	} 	        
        }

        if ( !(i % PRINT_AFTER_ITERATION_NUMBER) )
        {
        	int percent = (numberCorrect/static_cast<float>(images.size()))*100.0f;
        	printf("Percent correct = %d\n\n",percent);
        }
    }

	//std::shared_ptr<float> output = seqNet.GetOutput(outWidth,outHeight,outDepth);
	//printf("Width %d, height %d, depth %d, value = %f\n",outWidth,outHeight, outDepth, *output );

	/*
	// Create a new a array to transfrom the 1d result into 3d colour space
	std::shared_ptr<float> paddedOut { new float[outWidth*outHeight*3]};

    for (int y = 0; y < height; ++y)
	{
        for (int x =0; x < width; ++x)
        {
			int index = y*width +x;
	        int newIndex = y*width*3 +x*3;

			paddedOut.get()[newIndex] = output.get()[index];
			paddedOut.get()[newIndex+1] = output.get()[index];
			paddedOut.get()[newIndex+1] = output.get()[index];
		}
	}


	savePNG("result.png",paddedOut,width,height);
	*/
	return 0;
}