#ifndef MAX_POOLING_H
#define MAX_POOLING_H
#include "Network.h"

class MaxPooling: public Network
{
public:

	MaxPooling(int areaSize, Network * network);
	
	void Predict(std::shared_ptr<float> data, int width,int height, int depth) override;

	void Train(std::shared_ptr<float> input,std::shared_ptr<float> outputGradients, float label) override;

private:
	std::unique_ptr<int> m_Indices;
	int m_AreaSize;
	int m_InputVolumeSize;
	int m_OutputVolumeSize;

};


#endif