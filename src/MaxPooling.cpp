#include "MaxPooling.h"
#include <float.h>
#include <assert.h> 


MaxPooling::MaxPooling(int areaSize, Network * network): 
	Network(network->GetOutputWidth()/areaSize,
			network->GetOutputHeight()/areaSize,
			network->GetOutputDepth()),
	m_AreaSize(areaSize)
{ 
	// Calculate the volume of the index output by this layer.
	m_OutputVolumeSize =m_OutputWidth*m_OutputHeight*m_OutputDepth;

	// Calculate the size of the volume coming in from previous layer
	m_InputVolumeSize = network->GetOutputWidth()*network->GetOutputHeight()*m_OutputDepth;

	m_Indices = std::unique_ptr<int> (new int [m_OutputVolumeSize]);
	m_pGradients = std::shared_ptr<float> ( new float[m_InputVolumeSize]);
}



void MaxPooling::Predict(std::shared_ptr<float> data, int width,int height, int depth)
{
	assert( m_OutputWidth == width/m_AreaSize && "Output width doesn't match input width/areaSize!" );
	assert( m_OutputHeight == width/m_AreaSize && "Output height doesn't match input height/areaSize!"); 
	assert( m_OutputDepth == depth && "Output depth doesn't match input depth!");

    m_pOutputBuffer = std::shared_ptr<float>( new float[m_OutputVolumeSize]);
	memset(m_pOutputBuffer.get(),0,m_OutputVolumeSize*sizeof(float));
	memset(m_Indices.get(),0,m_OutputVolumeSize*sizeof(int));

	for (int z =0; z < depth; ++z)
	{
		for (int x = 0; x < width; x += m_AreaSize)
		{
			for (int y = 0; y < height; y += m_AreaSize)
			{
				float max = FLT_MIN_EXP;

				// The index of the max point in the original image.
				int maxIndex = 0;
				for (int u = 0; u < m_AreaSize; ++u)
				{
					for (int v = 0; v  < m_AreaSize; ++v)
					{
						// Find the index in the input image
						int imageIndex = z*height*width +(y+v)*width + (x+u);

						if ( data.get()[imageIndex] > max)
						{
							max = data.get()[imageIndex];
							maxIndex = imageIndex;
						}
					}
				}
				
				// Calculate the index for the resulting buffer
				int newIndex = z*m_OutputHeight*m_OutputWidth + (y/m_AreaSize)*m_OutputWidth + x/m_AreaSize;
				m_pOutputBuffer.get()[newIndex] = max;
				m_Indices.get()[newIndex] = maxIndex;
			}
		}
	}
}


void MaxPooling::Train(std::shared_ptr<float> input,std::shared_ptr<float> outputGradients, float label)
{
	memset(m_pGradients.get(),0.0f,sizeof(float)*m_InputVolumeSize);

	for (int outputVolumeIndex = 0; outputVolumeIndex <m_OutputVolumeSize; ++outputVolumeIndex)
	{
		int inputIndex = m_Indices.get()[outputVolumeIndex];

		assert( inputIndex < m_InputVolumeSize && "Indice must be within bounds of input volume");
		m_pGradients.get()[inputIndex] = outputGradients.get()[outputVolumeIndex];
	}
}


