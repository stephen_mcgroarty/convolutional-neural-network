#ifndef NETWORK_H
#define NETWORK_H
#include <vector>
#include <memory>

class Network
{
public:
	Network(int outWidth,int outHeight,int outDepth);
	virtual void Predict(std::shared_ptr<float> data, int width,int height, int depth);
	std::shared_ptr<float> GetOutput(int &width,int &height, int &depth) const;


	void Train(std::shared_ptr<float> input,int inWidth, int inHeight, int inDepth, float label);

	virtual void Train(std::shared_ptr<float> input,std::shared_ptr<float> outputGradients, float label);

	void AddConvolution(int numberOfFilters, int filterSize,int stride);
	void AddMaxPooling(int areaSize);
	void AddLinearClassifier();

	int GetOutputWidth() const  { return m_OutputWidth; }
	int GetOutputHeight() const { return m_OutputHeight; }
	int GetOutputDepth() const  { return m_OutputDepth; }

	bool IsTrainable() const { return m_Trainable; }

	float GetError() const { return m_Error; }

	std::shared_ptr<float> GetGradients() const { return m_pGradients;}

protected:
	int m_OutputWidth,m_OutputHeight,m_OutputDepth;
	std::shared_ptr<float> m_pOutputBuffer;
	bool m_Trainable;
	float m_Error;
	std::shared_ptr<float> m_pGradients;


private:

	std::vector<std::unique_ptr<Network>> m_NeuralNet;
};


#endif